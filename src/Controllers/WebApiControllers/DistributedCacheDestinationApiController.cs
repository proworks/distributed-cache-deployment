﻿using System;
using System.Web.Http;
using Umbraco.Web.Cache;
using Umbraco.Web.WebApi;

namespace DistributedCacheDeployment.Controllers.WebApiControllers
{
    public class DistributedCacheDestinationApiController : UmbracoApiController
    {
        [HttpGet]
        public bool Deploy(int id, string cacheTypes, bool unpublish = false)
        {
            var success = true;

            // One of:
            // $scope.cacheTypes = ["umbracoxmlrefresh", "umbracopublish", "umbracoexamineindex";
            // $scope.cacheLabels = ["Umbraco XML Refresh", "Umbraco Publish", "Umbraco Examine Reindex"];

            var caches = cacheTypes.Split(',');

            foreach (string cache in caches)
            {
                switch (cache)
                {
                    case "umbracoxmlrefresh":
                        DistributedCache.Instance.RefreshAll(new Guid(DistributedCache.PageCacheRefresherId));
                        break;
                    case "umbracoexamineindex":
                        // TODO: Implement
                        break;
                    case "umbracopublish":
                        if (unpublish)
                        {
                            DistributedCache.Instance.Remove(new Guid(DistributedCache.PageCacheRefresherId), id);
                        }
                        else
                        {
                            DistributedCache.Instance.Refresh(new Guid(DistributedCache.PageCacheRefresherId), id);
                        }
                        
                        break;
                }
            }

            return success;
        }

        [HttpGet]
        public string RefreshXmlForId(int id)
        {
            DistributedCache.Instance.Refresh(new Guid(DistributedCache.PageCacheRefresherId), id);
            return "<p>Refreshed</p>";
        }

        [HttpGet]
        public string RefreshXmlForIdWithChildren(int id)
        {
            DistributedCache.Instance.Refresh(new Guid(DistributedCache.PageCacheRefresherId), id);
            return "<p>Refreshed</p>";
        }

        [HttpGet]
        public string RefreshAllXml()
        {
            DistributedCache.Instance.RefreshAll(new Guid(DistributedCache.PageCacheRefresherId));
            return "<p>Refreshed All</p>";
        }
    }
}
