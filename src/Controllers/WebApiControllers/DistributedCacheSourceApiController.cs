﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DistributedCacheDeployment.Models.Configuration;
using Umbraco.Web.WebApi;

namespace DistributedCacheDeployment.Controllers.WebApiControllers
{
    public class DistributedCacheSourceApiController : UmbracoApiController
    {
        [HttpGet]
        public IEnumerable<string> GetUpdateServers()
        {
            var settings = ConfigurationManager.GetSection("distributedcachedeployment") as DistributedCacheDeploymentConfig;
            return settings == null ? new List<string>() : settings.Servers.AllKeys.ToList();
        }

        [HttpGet]
        public bool Deploy(string host, int id, string cacheTypes)
        {
            var success = true;

            var node = Umbraco.TypedContent(id);
            var queryString = String.Format("id={0}&cacheTypes={1}&unpublish={2}", id, cacheTypes, node == null ? "true" : "false");
            var url = string.Format("http://{0}/{1}?{2}", host, "umbraco/Api/DistributedCacheDestinationApi/Deploy", queryString);

            var ar = WebRequest.Create(url).BeginGetResponse(DependentHostCacheClearRequestCallback, url);

            return success;
        }

        private static void DependentHostCacheClearRequestCallback(IAsyncResult ar)
        {
        }
    }
}
