﻿using System.Configuration;

namespace DistributedCacheDeployment.Models.Configuration
{
    public class DistributedCacheDeploymentConfig : ConfigurationSection
    {
        [ConfigurationProperty("servers")]
        public NameValueConfigurationCollection Servers
        {
            get { return this["servers"] as NameValueConfigurationCollection; }
        } 
    }
}