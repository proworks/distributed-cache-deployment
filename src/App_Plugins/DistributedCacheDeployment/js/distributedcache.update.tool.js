angular.module('umbraco').controller('ProWorksCacheUpdateToolController', function ($scope, proWorksCacheUpdateService, notificationsService, editorState) {

	//--------------------------------------------------------------------------------------
	// Event Handlers
	//--------------------------------------------------------------------------------------

    var data = {
        type: $scope.model.config.defaultCacheRefresh,
        updated: ""
    };

    $scope.hasCacheUpdatedDate = false;
    $scope.cacheUpdatedDate = "";
    $scope.hasHosts = true;
    $scope.hosts = [];

    $scope.init = function () {
        if (!$scope.model.value) {
            $scope.model.value = data;
        }
        if ($scope.model.value.updated) {
            $scope.hasCacheUpdatedDate = true;
            $scope.cacheUpdatedDate = getDateFromFormat($scope.model.value.updated, "yyyy-M-d H:m:s");          
        }

        var promiseServers = proWorksCacheUpdateService.getDependentHosts();
        promiseServers.then(function (hosts) {

            if (hosts.length > 0) {
                $scope.hosts = hosts;
            } else {
                $scope.hasHosts = false;
            }

        }, function (reason) {
            $scope.hasHosts = false;
        });

    };

    $scope.init();

	/**
	 * @ngdoc method
	 * @name refreshCache
	 * @function
	 * 
	 * @description
	 * Refresh the external cache
	 */
    $scope.refreshCache = function () {

        if ($scope.hasHosts) {

            for (var h = 0; h < $scope.hosts.length; h++) {

                var host = $scope.hosts[h];

                var promiseUpdate = proWorksCacheUpdateService.updateDependentServers(host, editorState.current.id, $scope.model.config.defaultCacheRefresh);
                promiseUpdate.then(function(success) {

                    if (success) {
                        notificationsService.success("Deploy Succeeded ");
                        $scope.hasCacheUpdatedDate = true;
                        $scope.cacheUpdatedDate = new Date();
                        $scope.model.value.updated = $scope.formatDateToString($scope.cacheUpdatedDate);
                    } else {
                        notificationsService.error("Deploy Failed", reason.message);
                    }

                }, function(reason) {
                    notificationsService.error("Deploy Failed", reason.message);
                });
                
            }

        } else {
            notificationsService.error("No hosts to deploy to found.", reason.message);
        }
    };


    $scope.formatDateToString = function (d) {
        var dateString = [];

        dateString.push(d.getFullYear());
        dateString.push("-");
        dateString.push(d.getMonth());
        dateString.push("-");
        dateString.push(d.getDate());
        dateString.push(" ");
        dateString.push(d.getHours());
        dateString.push(":");
        dateString.push(d.getMinutes());
        dateString.push(":");
        dateString.push(d.getSeconds());

        return dateString.join("");
    }
});