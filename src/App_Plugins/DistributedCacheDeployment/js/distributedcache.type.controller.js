﻿angular.module('umbraco').controller('ProWorksCacheTypeController', function ($scope) {
    $scope.model.value = $scope.model.value || "umbracoxmlrefresh";

    $scope.cacheTypes = ["umbracoxmlrefresh", "umbracopublish", "umbracoexamineindex"];
    $scope.cacheLabels = ["Umbraco XML Refresh", "Umbraco Publish", "Umbraco Examine Reindex"];
    $scope.checkedCacheTypes = $scope.model.value.split(",");

    $scope.toggleCheck = function (cacheType) {
        if ($scope.checkedCacheTypes.indexOf(cacheType) === -1) {
            $scope.checkedCacheTypes.push(cacheType);
        } else {
            $scope.checkedCacheTypes.splice($scope.checkedCacheTypes.indexOf(cacheType), 1);
        }
        $scope.updateModel();
    };

    $scope.updateModel = function() {
        $scope.model.value = $scope.checkedCacheTypes.join(",");
    }
});