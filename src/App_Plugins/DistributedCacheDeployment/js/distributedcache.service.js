﻿angular.module('umbraco.resources').factory('proWorksCacheUpdateService', ['$q', '$http', 'umbRequestHelper', 'notificationsService', function ($q, $http, umbRequestHelper, notificationsService) {

    var cacheservice = {

            /**
            * @ngdoc method
            * @name getDependentHosts
            * @description Gets a list of hosts to update
            **/
            getDependentHosts: function () {

                return umbRequestHelper.resourcePromise(
                    $http({
                        url: '/umbraco/Api/ProWorksCacheUpdateApi/GetUpdateServers',
                        method: "GET"
                    }),
                    'Failed to retreive data for dependent servers');
            },

            /**
            * @ngdoc method
            * @name updateDependentServers
            * @description Updates the caches
            **/
            updateDependentServers: function (host, id, caches) {

                return umbRequestHelper.resourcePromise(
                    $http({
                        url: '/umbraco/Api/DistributedCacheSourceApi/Deploy',
                        params: { host: host, id: id, cacheTypes: caches },
                        method: "GET"
                    }),
                    'Failed to retreive data for dependent servers');
            }

        };

        return cacheservice;
    }
]);